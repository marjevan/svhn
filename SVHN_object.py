import math
import numpy
import os
import tensorflow as tf
import os.path

from tensorflow.python.ops import variables

from util import *
import pickle
import SVHN_DataSet
LOGDIR = 'log_final_final/'
GITHUB_URL = 'https://raw.githubusercontent.com/mamcgrath/TensorBoard-TF-Dev-Summit-Tutorial/master/'


class Net:
    def __init__(self):
        if not DEBUG:
            # don't print tensorflow debug messages
            os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
        ### MNIST EMBEDDINGS ###
        self.data_set = SVHN_DataSet.SVHN_dataset()

    @staticmethod
    def conv_layer(input, size_in, size_out, name="conv",kernel_size=[3,3]):
        with tf.name_scope(name):
            w = tf.Variable(tf.truncated_normal(kernel_size+[ size_in, size_out], stddev=0.1), name="W")
            b = tf.Variable(tf.constant(0.1, shape=[size_out]), name="B")
            conv = tf.nn.conv2d(input, w, strides=[1, 1, 1, 1], padding="SAME")
            act = tf.nn.relu(conv + b)
            normed_out = tf.contrib.layers.batch_norm(act,
                                                      center=True, scale=True,
                                                      scope=name)

            tf.summary.histogram("weights", w)
            tf.summary.histogram("biases", b)
            tf.summary.histogram("activations", act)
            return normed_out

    @staticmethod
    def conv_layer_dp(input, ifm_ofm,size_h_w, name="dp_conv"):
        with tf.name_scope(name):
            w = tf.Variable(tf.truncated_normal(size_h_w+[ifm_ofm, 1], stddev=0.1), name="W")
            b = tf.Variable(tf.constant(0.1, shape=[ifm_ofm]), name="B")
            conv = tf.nn.depthwise_conv2d(input, w, strides=[1, 1, 1, 1], padding="VALID")
            sum = conv + b


            tf.summary.histogram("weights", w)
            tf.summary.histogram("biases", b)
            tf.summary.histogram("activations", sum)
            return sum

    @staticmethod
    def pooling_layer(input, name="pool"):
        with tf.name_scope(name):
            return tf.nn.max_pool(input, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")

    @staticmethod
    def pooling_layer_avg(input, name="pool"):
        with tf.name_scope(name):
            x = tf.reduce_mean(input, axis=[1, 2])
            print(tf.shape(x))
            return x

    @staticmethod
    def drop_out_layer(input, prob, name="drop"):
        with tf.name_scope(name):
            return tf.nn.dropout(x=input, keep_prob=prob, name=name)

    @staticmethod
    def fc_layer(input, size_in, size_out, name="fc"):
        with tf.name_scope(name):
            w = tf.Variable(tf.truncated_normal([size_in, size_out], stddev=0.1), name="W")
            b = tf.Variable(tf.constant(0.1, shape=[size_out]), name="B")
            act = tf.nn.relu(tf.matmul(input, w) + b)
            tf.summary.histogram("weights", w)
            tf.summary.histogram("biases", b)
            tf.summary.histogram("activations", act)
            return act

    def mnist_model(self, learning_rate):
        tf.reset_default_graph()
        self.phase = True  # =  tf.placeholder(tf.bool, name='phase')
        # Setup placeholders, and reshape the data
        self.x = tf.placeholder(tf.float32, shape=[None, 32,32,3], name="x")
        self.acc_vec = tf.placeholder(tf.float32, shape=[None], name="acc_vec")
        self.prob_scalar = tf.placeholder(tf.float32,shape=(), name="prob_scalar")
        tf.summary.image('input', self.x, 100)
        self.y = tf.placeholder(tf.float32, shape=[None, 10], name="labels")

        conv1_sq = self.conv_layer(self.x, 3, 128, "conv1_sq")
        conv1_h = self.conv_layer(self.x, 3, 128, "conv1_h",kernel_size=[7,1])
        conv1_w = self.conv_layer(self.x, 3, 128, "conv1_w", kernel_size=[1, 7])
        conv1_concat = tf.concat([conv1_sq, conv1_h ,conv1_w,self.x], axis=3)

        conv1_b = self.conv_layer(conv1_concat, 387,512, "conv1_b")
        conv1_C = self.conv_layer(conv1_b, 512,512, "conv1_c")
        pool1 = self.pooling_layer(conv1_C, "pool1")

        conv2_a = self.conv_layer(pool1, 512, 512, "conv2_a")
        conv2_b = self.conv_layer(conv2_a, 512, 512, "conv2_b")
        conv2_C = self.conv_layer(conv2_b, 512, 1024, "conv2_c")
        depth_wise_conv = self.conv_layer_dp(conv2_C,1024,[16,16], "depth_wise")

        self.flattened = tf.reshape(depth_wise_conv, [-1, 1024])
        self.prob = tf.Variable(0.4, name="prob")
        dp = self.drop_out_layer(self.flattened, self.prob_scalar, "drop")

        self.logits = self.fc_layer(dp, 1024, 10, "fc2")

        with tf.name_scope("cross_entropy"):
            cross_entropy = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(
                    logits=self.logits, labels=self.y), name="cross_entropy")
            tf.summary.scalar("cross_entropy", cross_entropy)

        with tf.name_scope("train"):
            self.train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)

        with tf.name_scope("accuracy"):
            correct_prediction = tf.equal(tf.argmax(self.logits, 1), tf.argmax(self.y, 1))
            self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
            tf.summary.scalar("accuracy", self.accuracy)

        self.summ = tf.summary.merge_all()


        self.acc_vec_updated = tf.concat([self.acc_vec, [self.accuracy]], axis=0)
        self.acc_full = tf.reduce_mean(self.acc_vec_updated)
        self.acc_full_log = tf.summary.scalar("accuracy_full", self.acc_full)
        self.acc_full_log_summery = tf.summary.merge([self.acc_full_log], name="acc")

        config = tf.ConfigProto()
        debug_print("don't" if not tf.device('/gpu:0') else "" + "recognized GPU")
        with tf.device('/gpu:0'):
            config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.saver = tf.train.Saver(variables._all_saveable_objects())

        if (os.path.isfile(os.path.join(LOGDIR, "step"))):
            self.saver.restore(self.sess, tf.train.latest_checkpoint(LOGDIR))
        else:
            self.sess.run(tf.global_variables_initializer())

        self.writer = tf.summary.FileWriter(LOGDIR)
        self.writer.add_graph(self.sess.graph)

    def train(self):
        if os.path.exists(os.path.join(LOGDIR, "step")):
            with open(os.path.join(LOGDIR, "step"), 'rb') as file:
                step = pickle.load(file)
        else:
            step = 0
        for i in range(2000 * 50):
            batch = self.data_set.train.next_batch(100)

            if (i + step) % 5 == 0:

                [s] = self.sess.run([ self.summ,],
                                        feed_dict={
                                            self.x: batch[0],
                                            self.y: batch[1],
                                            self.acc_vec: [],
                                            self.prob_scalar: 0.4})
                self.writer.add_summary(s, (i + step))

            if (i + step) % 2500 == 0:
                self.run_inference_on_test_set(i, step)

                self.saver.save(self.sess, os.path.join(LOGDIR, "model.ckpt"), (i + step))


            with open(os.path.join(LOGDIR, "step"), 'wb') as file:
                pickle.dump((i + step), file)

            ## Train
            self.sess.run(self.train_step, feed_dict={
                                            self.x: batch[0],
                                            self.y: batch[1],
                                            self.acc_vec: [],
                                            self.prob_scalar: 0.4})

    def run_inference_on_test_set(self, i, step):
        print("------------------step", i + step, "running inference -----------------------")
        embedings = None
        acc_vec = numpy.asanyarray([], dtype=numpy.float32)
        for eval_batch_idx in range(261):
            if eval_batch_idx == 260:
                batch_test = self.data_set.test.next_batch(32)
            else:
                batch_test = self.data_set.test.next_batch(100)
            [accuracy_last, acc_vec, sumery_for_log, acc_full , flattened] = self.sess.run(
                [self.accuracy, self.acc_vec_updated, self.acc_full_log_summery, self.acc_full, self.flattened],
                feed_dict={self.x: batch_test[0],
                           self.y: batch_test[1],
                           self.acc_vec: acc_vec,
                           self.prob_scalar: 1})
            print("last:", accuracy_last, "accomulated:", acc_full, "eval_batch_idx:", eval_batch_idx, "/260")

        self.writer.add_summary(sumery_for_log, (i + step))
        print("------------------step", i + step, "inference result:", acc_full, "-----------------------")

    def eval(self,img):
        self.prob.assign(1)
        [train_accuracy, logits] = self.sess.run([self.accuracy, self.logits],
                                                 feed_dict={self.x: [img], self.y: [[0] * 10]})
        debug_print(logits)
        eval_list = logits.tolist()[0]

        eval_val = eval_list.index(max(eval_list))

        softmax = False
        if softmax:
            sum_list = (sum([math.exp(x) for x in eval_list]))
            eval_list_percentage = [(math.exp(x) / sum_list) * 100 for x in eval_list]
        else:
            sum_list = (sum(eval_list))
            eval_list_percentage = [(x / sum_list) * 100 for x in eval_list]

        index = 0
        stat_res = ""
        for num in eval_list_percentage:
            stat_res = stat_res + 'Digit' + str(index) + ': ' + str(round(num, 1)) + "%\n"
            index += 1
        debug_print(stat_res)
        debug_print("The number is: " + str(eval_val))

        return eval_val, stat_res



def train_mnist_CNN():
    learning_rate = 1E-4
    net = Net()
    net.mnist_model(learning_rate)
    net.train()

def eval(img):
    reshaped_img = []
    net = Net()
    for row in img:
        for col in row:
            reshaped_img.append(col)
    net.mnist_model(0)

    # evaluate for the receiving img
    return net.eval(reshaped_img)



if __name__ == '__main__':
    train_mnist_CNN()
