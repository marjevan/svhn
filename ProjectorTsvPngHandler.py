import os

import cv2
import SVHN_DataSet
import numpy
class Png:
    def __init__(self):
        self.data_set = SVHN_DataSet.SVHN_dataset(False)
        img_data = self.data_set.test.next_batch(26000)
        final_image = None
        str_data =""
        for i in range(130):
            row = img_data[0][i*200]
            for j in range(200):
                row = numpy.concatenate((row,img_data[0][(i*200)+(j)]),axis=1)
                str_data =str_data +str(img_data[1][(i*200)+(j)][0])+ "\n"
            if i==0:
                final_image = row
            else:
                final_image = numpy.concatenate((final_image, row), axis=0)
            print("line idx:",i)
        print(final_image.shape)
        cv2.imwrite(os.path.join(os.path.dirname(__file__), "svhn_sprite_26k.png"), final_image)
        with open( os.path.join(os.path.dirname(__file__), "svhn_labels_26k.tsv"), 'w+') as f:
            f.write(str_data)


Png()