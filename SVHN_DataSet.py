import scipy.io
import numpy
import os


class SVHN_dataset:
    def __init__(self,norm = True):
        self.test = SVHN_loader(os.path.join(os.path.dirname(__file__), "test_32x32.mat"),norm=norm)
        train = SVHN_loader(os.path.join(os.path.dirname(__file__), "train_32x32.mat"),norm=norm)
        extra = SVHN_loader(os.path.join(os.path.dirname(__file__), "train_32x32.mat"),norm=norm)
        self.train = SVHN_loader.marge(train,extra)
class SVHN_loader:
    def __init__(self,path,norm = True):
        if path != None:
            data_row = scipy.io.loadmat(path)
            self.images = numpy.moveaxis(data_row["X"],-1,0).astype(dtype=numpy.float32)
            data_row["y"] = [ x % 10 for x in data_row["y"]]
            if(norm):
                self.images = self.images - 128
                self.images = self.images /127
                self.labels =numpy.asarray([ numpy.asarray([(1 if x == logit else 0) for logit in range(0,10) ],dtype=numpy.float32) for x in data_row["y"]])
            else:
                self.labels = data_row["y"]
        else:
            self.images =None
            self.labels = None

        self.current = 0

    def next_batch(self,num):
        if self.current+num<len(self.images):
            batch = self.images[self.current:self.current+num],self.labels[self.current:self.current+num]
            self.current += num
        else:
            res = num - (len(self.images)-self.current)
            batch = numpy.asarray(list(self.images[self.current:]) + list(self.images[:res])), \
                    numpy.asarray(list(self.labels[self.current:]) + list(self.labels[:res]))
            self.current = res
        return batch

    def print_shapes(self):
       print("self.images",self.images.shape)
       print("self.labels", self.labels.shape)

    @staticmethod
    def marge(first,second):
        assert isinstance( first, SVHN_loader), "first shuold be a loader"
        assert isinstance( second, SVHN_loader), "first shuold be a loader"
        merged = SVHN_loader(None)
        merged.images = numpy.concatenate((first.images,second.images))
        merged.labels = numpy.concatenate((first.labels, second.labels))
        return merged
def main():
    obj = SVHN_loader()
    obj.print_shapes()

if __name__ == '__main__':
    main()